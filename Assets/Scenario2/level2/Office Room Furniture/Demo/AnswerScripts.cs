﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnswerScripts : MonoBehaviour
{
    public bool isCorrect = false;
    public QuizzManagers quizzManager; 
    public void Answer () 
    {
        if (isCorrect) 
        {
            Debug.Log ( "Correct Answer") ; 
            quizzManager.correct();

        }
        else
        {
            Debug.Log ( " Wrong Answer ") ; 
            quizzManager.wrong();
        }
    }





}
