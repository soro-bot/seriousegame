﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VendeurDialogue : MonoBehaviour
{
   private bool conversation2 = false ; 

    public Text TextVendeur;
    public Text TextMercie;
    public Text TextFinargent;
    public Image PanelVendeur ; 
    

    void OnTriggerEnter(Collider other )
    {
        if (other.gameObject.tag == "Player")
        {
            conversation2 = true ; 
            TextVendeur.GetComponent<Text>().enabled = true ;
            PanelVendeur.GetComponent<Image>().enabled = true ; 
        }
    }

    void OnTriggerExit(Collider other )
    {
        if (other.gameObject.tag == "Player")
        { 
            conversation2 = false ; 
            TextVendeur.GetComponent<Text>().enabled = false ; 
            PanelVendeur.GetComponent<Image>().enabled = false ; 
        }
    }
    

    void Update () {
        if (conversation2 )
        {
            Stats2 stats = GameObject.Find ("CanvasStats2").GetComponent<Stats2>();

            if(Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log("la toucche passe bien ");
                if (stats.somme > 300000 )
                {
                    stats.somme -= 300000;
                    stats.UpdateStats(); 
                    TextVendeur.enabled = false ;
                    TextMercie.enabled= true;
                    PanelVendeur.enabled = true ;
                    StartCoroutine(NewQuestion()); 
                }
                else
                {
                    TextVendeur.enabled = false ;
                    TextFinargent.enabled = true ; 
                    PanelVendeur.enabled = true;
                    StartCoroutine(NewQuestion());
                }
            }

             if(Input.GetKeyDown(KeyCode.B))
            {
                Debug.Log("la toucche passe bien ");
                if (stats.somme > 1000000 )
                {
                    stats.somme -= 1000000;
                    stats.UpdateStats(); 
                    TextVendeur.enabled = false ;
                    TextMercie.enabled= true;
                    PanelVendeur.enabled = true ;
                    StartCoroutine(NewQuestion()); 
                }
                else
                {
                    TextVendeur.enabled = false ;
                    TextFinargent.enabled = true ;
                    PanelVendeur.enabled = true ; 
                    StartCoroutine(NewQuestion());
                }
            }

             if(Input.GetKeyDown(KeyCode.C))
            {
                Debug.Log("la toucche passe bien ");
                if (stats.somme > 600000 )
                {
                    stats.somme -= 600000;
                    stats.UpdateStats(); 
                    TextVendeur.enabled = false ;
                    TextMercie.enabled= true;
                    StartCoroutine(NewQuestion()); 
                }
                else
                {
                    TextVendeur.enabled = false ;
                    TextFinargent.enabled = true ; 
                    PanelVendeur.enabled = true ; 
                    StartCoroutine(NewQuestion());
                }
            }
             if(Input.GetKeyDown(KeyCode.D))
            {
                Debug.Log("la toucche passe bien ");
                if (stats.somme > 800000 )
                {
                    stats.somme -= 800000;
                    stats.UpdateStats(); 
                    TextVendeur.enabled = false ; 
                    TextMercie.enabled= true;
                    PanelVendeur.enabled = true ; 
                    StartCoroutine(NewQuestion()); 
                }
                else
                {
                    TextVendeur.enabled = false ;
                    TextFinargent.enabled = true ;
                    PanelVendeur.enabled = true ;  
                    StartCoroutine(NewQuestion());
                }
            }
        }
    }

    IEnumerator NewQuestion()
    {
        conversation2 = false;
        yield return new WaitForSeconds (3f);
        TextMercie.enabled = false;
        TextFinargent.enabled = false;
        TextVendeur.enabled = true;
        conversation2 = true ; 
        PanelVendeur.enabled = true ; 
        
    }
}
