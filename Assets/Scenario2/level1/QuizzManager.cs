﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuizzManager : MonoBehaviour
{
   public List <QuestionAnswers> QnA ;
   public GameObject[] options ;
   public int currentQuestion ;

   public GameObject Quizzpanel;
   public GameObject GOPanel ;

   public Text QuestionTxt ;
   public Text ScoreTxt;

   int totalQuestion = 0 ;
   public int score ;


 
   private void Start ()
   {
        totalQuestion = QnA.Count ; 
        GOPanel.SetActive (false);
        GenerateQuestion () ; 
   }

    public void retry ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void GameOver ()
    {
         Quizzpanel.SetActive (false);
         GOPanel.SetActive (true);
         ScoreTxt.text = score + "/" + totalQuestion; 
    }
   public void correct ()
    {
        score += 1 ;
        QnA.RemoveAt (currentQuestion);
        GenerateQuestion() ; 
    }

    public void wrong () 
    {
        //Quand ta une mauvaise reponse
        QnA.RemoveAt (currentQuestion);
        GenerateQuestion() ; 
    }



   void setAnswers () 
   {
       for (int i = 0; i < options.Length; i++)
       {
           options[i].GetComponent<AnswerScript1>().isCorrect = false;
           options[i].transform.GetChild(0).GetComponent<Text>().text = QnA[currentQuestion].Answers[i];

           if (QnA [currentQuestion].CorrectAnswer == i+1 )
           {
               options[i].GetComponent<AnswerScript1>().isCorrect = true;
           }            
      }
   }

   void GenerateQuestion ()
   {
    
    if (QnA.Count > 0)
    {
    currentQuestion = Random.Range(0, QnA.Count);

    QuestionTxt.text = QnA [currentQuestion].Question;
    setAnswers();
    }

    else
    {
        Debug.Log (" Out of Questions "); 
        GameOver();
    }
    
    
    
   }
}
