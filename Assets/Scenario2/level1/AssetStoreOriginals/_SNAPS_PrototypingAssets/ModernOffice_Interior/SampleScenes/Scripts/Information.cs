﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Information : MonoBehaviour
{
    public Text TextInformation;
    public Image PanelText;
    

    void OnTriggerEnter(Collider other )
    {
        if (other.gameObject.tag == "Player")
        {
            TextInformation.GetComponent<Text>().enabled = true ; 
            PanelText.GetComponent<Image>().enabled = true ;
            
        }
    }

    void OnTriggerExit(Collider other )
    {
        if (other.gameObject.tag == "Player")
        {
            TextInformation.GetComponent<Text>().enabled = false ; 
            PanelText.GetComponent<Image>().enabled = false ;
            
        }
    }
    





    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
