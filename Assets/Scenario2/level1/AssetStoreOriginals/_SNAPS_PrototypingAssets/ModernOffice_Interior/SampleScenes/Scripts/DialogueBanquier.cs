﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBanquier : MonoBehaviour
{

    private bool conversation = false ; 

    public Text TextDialogueBanquier;
    public Text TextMerciBanquier;
    public Image PanelBanquier ; 
    public Image Button1er;
    public Text Text1er ; 

    void OnTriggerEnter(Collider other )
    {
        if (other.gameObject.tag == "Player")
        {
            conversation = true ; 
            TextDialogueBanquier.GetComponent<Text>().enabled = true ; 
            PanelBanquier.GetComponent<Image>().enabled = true ; 
            
        }
    }

    void OnTriggerExit(Collider other )
    {
        if (other.gameObject.tag == "Player")
        { 
            conversation = false ; 
            TextDialogueBanquier.GetComponent<Text>().enabled = false ; 
            PanelBanquier.GetComponent<Image>().enabled = false ;
             Button1er.GetComponent<Image>().enabled = false ;
        }
    }
    

    void Update () {
        if (conversation )
        {
            StatsPlayer stats = GameObject.Find ("CanvasStats").GetComponent<StatsPlayer>();

            if(Input.GetKeyDown(KeyCode.A))
            {
                if (stats.argent < 2000000 )
                {
                    stats.argent += 2000000;
                    stats.UpdateStats(); 
                    TextDialogueBanquier.enabled = false ;
                    TextMerciBanquier.enabled = true ; 
                    Button1er.enabled = true ;
                    Text1er.enabled = true ;
                    StartCoroutine(Nouvellequestion()); 
                }
            }
        }
    }

    IEnumerator Nouvellequestion()
    {
        conversation = false;
        yield return new WaitForSeconds (20f);
        TextMerciBanquier.enabled = false ;
        PanelBanquier.enabled = false ;
        Text1er.enabled = false ; 
        Button1er.enabled = false ;
        
    }
    // Update is called once per frame
    
}
