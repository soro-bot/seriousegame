﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueVoisin : MonoBehaviour
{
    private bool conversation3 = false ;
    public Text TextVoisin ; 
    public Text TextQuiz;
    public Image PanelVoisin ; 
    


    void OnTriggerEnter(Collider other )
    {
        if (other.gameObject.tag == "Player")
        {
            conversation3 = true ; 
            TextVoisin.GetComponent<Text>().enabled = true ;
            PanelVoisin.GetComponent<Image>().enabled = true ;
             
        }
    }

    void OnTriggerExit(Collider other )
    {
        if (other.gameObject.tag == "Player")
        {
            conversation3 = false ; 
            TextVoisin.GetComponent<Text>().enabled = false ; 
             
        }
    }

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (conversation3)
        {
           if(Input.GetKey(KeyCode.Space))
           {
               TextVoisin.enabled = false ; 
               TextQuiz. enabled = true ; 
              
           }
        }
    }
}
